from conans import ConanFile, CMake, tools
import os

class PdcursesprojectplutowinconConan(ConanFile):
    name = "pdcurses-projectpluto-wincon"
    version = "4.1.0"
    license = "CC0-1.0"
    author = "Tanvas Inc <software@tanvas.co>"
    url = "https://bitbucket.org/tanvas/conan-pdcurses-projectpluto"
    homepage = "https://github.com/Bill-Gray/PDCurses"
    description = "Project Pluto fork of pdcurses (wincon backend)"
    topics = ("tui", "curses")
    settings = { "os": "Windows", "compiler": None, "build_type": None, "arch": None }
    options = {
        "shared": [True, False],
        "force_utf8": [True, False],
    }
    default_options = {
        "shared": False,
        "force_utf8": True,
    }
    generators = "cmake"

    def source(self):
        git = tools.Git()
        git.clone("https://github.com/Bill-Gray/PDCurses", shallow=True, branch="v{}".format(self.version))

    def _configure_cmake(self):
        cmake = CMake(self)
        cmake.definitions["CMAKE_BUILD_TYPE"] = self.settings.build_type
        cmake.definitions["PDC_BUILD_SHARED"] = self.options.shared
        cmake.definitions["PDC_UTF8"] = self.options.force_utf8
        cmake.definitions["PDC_SDL2_BUILD"] = False
        cmake.definitions["PDC_SDL2_DEPS_BUILD"] = False
        cmake.configure()

        return cmake

    def build(self):
        with tools.vcvars(self.settings):
            cmake = self._configure_cmake()
            cmake.build()

    def package(self):
        with tools.vcvars(self.settings):
            cmake = self._configure_cmake()
            cmake.install()

        self.copy("curses.h", dst="include")

    def package_info(self):
        self.cpp_info.libdirs += [os.path.join(str(self.settings.build_type), "lib", "wincon")]
        self.cpp_info.bindirs += [os.path.join(str(self.settings.build_type), "bin", "wincon")]

        if not self.options.shared:
            self.cpp_info.libs += ["pdcursesstatic"]
        else:
            self.cpp_info.libs += ["pdcurses"]
            self.cpp_info.defines += ["PDC_DLL_BUILD"]
